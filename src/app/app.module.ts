import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { IonicModule } from '@ionic/angular';
import { IonicStorageModule } from '@ionic/storage';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { FormsModule } from '@angular/forms';
import { BackendUrlsProvider } from './providers/backend-urls';
import { TokenProvider } from './providers/token';
import { StorageProvider } from './providers/storage';
import { UserProvider } from './providers/user';
import { ContentTypesProvider } from './providers/content-types';
import { LocationProvider } from './providers/location';
import { AuthInterceptor } from './providers/http-interceptor';
import { Geolocation } from '@ionic-native/geolocation/ngx';

@NgModule({
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    IonicModule.forRoot(),
    IonicStorageModule.forRoot(),
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production
    })
  ],
  declarations: [AppComponent],
  providers: [InAppBrowser, SplashScreen, StatusBar,
    TokenProvider,
    BackendUrlsProvider,
    StorageProvider,
    UserProvider,
    ContentTypesProvider,
    LocationProvider,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    Geolocation
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
