
// import * as moment from 'moment';
// import * as mtz from 'moment-timezone';
import {  isNotEmpty, toNumber } from '../shared/utils';



export abstract class AbstractEntity {
  public appId; APPID;
  constructor(public id: string, public typeId: string) { }
}


export class User extends AbstractEntity {
  private static emailPattern: RegExp = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
  private static phoneNoPattern: RegExp = /^\d{10}$/;
  public jwt: any;
  public username: any;
  public email: any;
  public contactno: any;
  public createdAt:any;

  constructor(public _user: any) {
    super(((_user && _user._id) ? _user._id :"5825d739f009c4bc37fd7bb8sdadasdas"), "5825d739f009c4bc37fd7bb8");
    if(_user){
      Object.keys(_user).forEach(key=>{this[key]=_user[key]})
      this.username = _user.username;
      this.email = _user.email;
      this.createdAt = _user.createdAt;
    }
  }

  initializeUser(_user: any) {
    if(_user){
      Object.keys(_user).forEach(key=>{this[key]=_user[key]})
      this.username = _user.username;
      this.email = _user.email;
      this.createdAt = _user.createdAt;
    }
  }

  isValid() {
    return this.isValidName() && this.isValidEmail() && this.isValidPhoneNumber();
  }

  isValidName() {
    if (this.username === undefined || this.username === null || this.username.trim() == '') {
      return false;
    }
    return true;
  }

  isValidEmail() {
    if (!User.emailPattern.test(this.email)) {
      return false;
    }
    return true;
  }

  getAccessToken() {
    if (this.jwt) {
      return this.jwt;
    } else { return null; }
  }

  static isValidEmail(email: string) {
    return User.emailPattern.test(email);
  }

  static isValidPhoneno(phoneno: string) {
    return User.phoneNoPattern.test(phoneno);
  }

  isValidPhoneNumber() {
    if (!User.phoneNoPattern.test(this.contactno)) {
      return false;
    }
    return true;
  }
}
