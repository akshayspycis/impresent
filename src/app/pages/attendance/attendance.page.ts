import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingController, NavController } from '@ionic/angular';
import { ContentTypesProvider } from '../../providers/content-types';
import { checkValidtion, isEmpty, isNotEmpty, presentToastCtrl, strapiErrorMsg } from '../../shared/utils';
import * as moment from 'moment';
import { User } from '../../modal/modal';
import { UserProvider } from '../../providers/user';
@Component({
  selector: 'app-attendance',
  templateUrl: './attendance.page.html',
  styleUrls: ['./attendance.page.scss'],
})
export class AttendancePage implements OnInit {
  viewtype: string = 'present';
  assignment: any;
  attendance: any;
  currentdate: any;
  isdisplay: boolean = false;
  contenttype: any;
  photos: any[] = [{}];
  locationpanel: any;
  errormsg: any;
  // @ViewChild('pageContent') pageContent: Content;
  contentId: string = 'application::attendance.attendance';
  panelfiled: any = [
    "name",
    "address",
    "geo",
  ]

  _fromdate: any;
  _todate: any;
  user: User;
  _intime: any;
  constructor(private route: ActivatedRoute, private router: Router, public contenttypesPvr: ContentTypesProvider,
    public loadingCtr: LoadingController, public userSvr: UserProvider, public navCtrl: NavController) {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.viewtype = this.router.getCurrentNavigation().extras.state.viewtype;
        this.assignment = this.router.getCurrentNavigation().extras.state.assignment;
        if (this.assignment) {
          this.attendance = this.assignment.attendanceid;
          if (this.attendance) {
            this._intime = new Date(this.attendance.fromdate);
            if (this.attendance.intime && isNotEmpty(this.attendance.intime.split(':'))) {
              this._intime.setHours(this.attendance.intime.split(':')[0]);
              this._intime.setMinutes(this.attendance.intime.split(':')[1]);
            }
          }
        }
        this.currentdate = new Date();
        this._fromdate = moment().format('YYYY-MM-DD');
        this._todate = moment().endOf('month').format('YYYY-MM-DD');
        console.log(this._fromdate, this._todate);
        this.loadContentType();
        this.user = this.userSvr.getCurrentUser();
      }
    });
  }

  ngOnInit() {

  }
  async loadContentType() {
    let _self = this;
    console.log("loadContentType")
    let loading = await this.loadingCtr.create()
    await loading.present();
    this.contenttypesPvr.loadContentType(this.contentId).then(res => {
      console.log(res);
      this.contenttype = res;
      this.mapFieldInClass(res);
      loading.dismiss();
      _self.isdisplay = true;
    }).catch(err => {
      console.log(err);
      presentToastCtrl(strapiErrorMsg(err));
      loading.dismiss();
    })

  }

  mapFieldInClass(res) {
    Object.keys(res).forEach(key => {
      this[key] = res[key];
    });
    if (res.images) res.images.values = this.photos;
    console.log(this);
    this['fromdate'].value = moment().format('YYYY-MM-DD');
    this['todate'].value = moment().format('YYYY-MM-DD');
  }

  checkValueExist(field, value) {
    if (isNotEmpty(field.values)) {
      return (field.values.findIndex(o => o.id == value.id) != -1)
    }
    return false;
  }

  getValidationStatus() {
    let _self = this;
    _self.errormsg = null;
    _self.errormsg = checkValidtion(_self.contenttype, _self.panelfiled);
    console.log(_self.errormsg);
    if (_self.errormsg) {
      presentToastCtrl(_self.errormsg);
    }
    return _self.errormsg;
  }

  askForLeave() {
    this["status"].value = "leave";
    this["employee"].value = this.user.id;
    this.submitAndNext();
  }

  markSignout() {
    this["outtime"].value = moment().format('HH:mm') + ":00.000";
    this.submitAndNext();
  }

  markAttendance() {
    console.log("moment().format('HH:mm');", moment().format('HH:mm'))
    this["status"].value = "present";
    this["employee"].value = this.user.id;
    this["todate"].value = null;
    this["intime"].value = moment().format('HH:mm') + ":00.000";
    this.submitAndNext();
  }

  async checkAnyRecordExist() {
    if (this["fromdate"].value && this["todate"].value) {
      let filter = [{ key: "fromdate_lte", value: this["fromdate"].value }, { key: "fromdate_gte", value: this["fromdate"].value }, { key: "status", value: this["status"].value }];
      let loading = await this.loadingCtr.create();
      return this.contenttypesPvr.loadContentTypeRecord(this.contentId, 10, 1, filter).then(res => {
        loading.dismiss();
        return res;
      }).catch(err => {
        loading.dismiss();
      })

    }
    return null;
  }

  async submitAndNext() {
    let _self = this;
    console.log(this.contenttype);
    let loading = await _self.loadingCtr.create();
    await loading.present();
    if (!_self.getValidationStatus()) {
      let data = this.contenttypesPvr.modifyDataFormContentType(this.contenttype);
      console.log(data);
      if (data) {
        let _data;
        if (this.viewtype == "leave") _data = await this.checkAnyRecordExist();
        console.log("_data", _data)
        if (this.viewtype == "present" || (this.viewtype == "leave" && isEmpty(_data))) {
          _self.contenttypesPvr.saveContent(data, _self.contentId).then(res => {
            loading.dismiss();
            console.log(res);
            _self.updateAssignmentStatus(res.id);
          }).catch(err => {
            loading.dismiss();
            console.log(err)
            presentToastCtrl(strapiErrorMsg(err));
          })
        } else if (this.viewtype == "signout") {
          this.attendance["outtime"] = moment().format('HH:mm') + ":00.000";
          _self.contenttypesPvr.updateContent(this.attendance, _self.contentId, this.attendance.id).then(res => {
            loading.dismiss();
            _self.updateAssignmentStatus(this.attendance.id);
          }).catch(err => {
            loading.dismiss();
            console.log(err)
            presentToastCtrl(strapiErrorMsg(err));
          })

        } else {
          loading.dismiss();
          presentToastCtrl("Leave Request already exist");
        }
      } else {
        loading.dismiss();
      }
    }
  }

  async updateAssignmentStatus(attendanceid) {
    let _self = this;
    let loading = await _self.loadingCtr.create();
    await loading.present();
    this.assignment.attendanceid = attendanceid;
    if (this.viewtype == "present") {
      this.assignment.status = "inprogress"
    } else if (this.viewtype == "leave") {
      this.assignment.status = "cancelled"
    } else {
      this.assignment.status = "completed"
    }
    _self.contenttypesPvr.updateContent(this.assignment, "application::assignment.assignment", this.assignment.id).then(res => {
      loading.dismiss();
      console.log(res);
      presentToastCtrl("Request Successfully created.", true, false);
      this.navigateRoot();
    }).catch(err => {
      loading.dismiss();
      console.log(err)
      presentToastCtrl(strapiErrorMsg(err));
    })
  }

  navigateRoot() {
    // this.router.navigateByUrl('/app/tabs/schedule');
    this.navCtrl.navigateRoot(['/app/tabs/schedule'])
  }

}
