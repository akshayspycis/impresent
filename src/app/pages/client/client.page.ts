import { Component, OnInit } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { ContentTypesProvider } from '../../providers/content-types';
import { LocationProvider } from '../../providers/location';
import { checkValidtion, isNotEmpty, presentToastCtrl, strapiErrorMsg } from '../../shared/utils';
declare var google;

@Component({
  selector: 'app-client',
  templateUrl: './client.page.html',
  styleUrls: ['./client.page.scss'],
})
export class ClientPage implements OnInit {
  isdisplay: boolean = false;
  map: any;
  marker: any;
  contenttype: any;
  photos: any[] = [{}, {}, {}, {}, {}, {}, {}, {}, {}, {}];
  locationpanel: any;
  errormsg: any;
  // @ViewChild('pageContent') pageContent: Content;
  contentId: string = 'application::client.client';
  panelfiled: any = [
    "name",
    "address",
    "geo",
  ]
  constructor(public locationPvr: LocationProvider, public contenttypesPvr: ContentTypesProvider,
    public loadingCtr: LoadingController) { }

  ngOnInit() {
    this.loadContentType();

  }
  async loadContentType() {
    let _self = this;
    console.log("loadContentType")
    let loading = await this.loadingCtr.create()
    await loading.present();
    this.contenttypesPvr.loadContentType(this.contentId).then(res => {
      console.log(res);
      this.contenttype = res;
      this.mapFieldInClass(res);
      loading.dismiss();
      this.isdisplay = true;
      setTimeout(() => {
        _self.loadMap();
      }, 100)

    }).catch(err => {
      console.log(err);
      presentToastCtrl(strapiErrorMsg(err));
      loading.dismiss();
    })

  }

  mapFieldInClass(res) {
    Object.keys(res).forEach(key => {
      this[key] = res[key];
    });
    if (res.images) res.images.values = this.photos;
    console.log(this)
  }

  checkValueExist(field, value) {
    if (isNotEmpty(field.values)) {
      return (field.values.findIndex(o => o.id == value.id) != -1)
    }
    return false;
  }

  async loadMap() {
    const _self = this;
    let _location = await _self.locationPvr.getCurrentPosition();
    console.log(_location);
    if (_self["geo"]) {
      _self["geo"].value = {
        lat: _location.latitude,
        lng: _location.longitude,
      }
    }
    let position = {
      lat: _location.latitude,
      lng: _location.longitude,
    }
    _self.map = new google.maps.Map(document.getElementById("map_canvas"), {
      center: position,
      zoom: 17,
      streetViewControl: false,
      rotateControl: false,
      scaleControl: true,
      fullscreenControl: false,
      panControl: true,
      draggingCursor: "pointer",
      disableDefaultUI: true,
      zoomControl: true
    });
    _self.marker = new google.maps.Marker({
      position: position,
      map: _self.map,
      title: `Current Position`,
      draggable: true,
    });
    _self.marker.addListener("dragend", (e) => {
      const lat = e.latLng.lat();
      const lng = e.latLng.lng();
      console.log(lat, lng);
      if (_self["geo"]) {
        _self["geo"].value = { lat, lng }
      }
    });
  }


  getValidationStatus() {
    let _self = this;
    _self.errormsg = null;
    _self.errormsg = checkValidtion(_self.contenttype, _self.panelfiled);
    console.log(_self.errormsg);
    if (_self.errormsg) {
      presentToastCtrl(_self.errormsg);
    }
    return _self.errormsg;
  }


  async submitAndNext() {
    let _self = this;
    console.log(this.contenttype);
    if (!_self.getValidationStatus()) {
      let data = this.contenttypesPvr.modifyDataFormContentType(this.contenttype);
      console.log(data)
      if (data) {
        let loading = await _self.loadingCtr.create();
        await loading.present();
        _self.contenttypesPvr.saveContent(data, _self.contentId).then(res => {
          presentToastCtrl("Client Successfully created.", true, false);
          loading.dismiss();
          _self.loadContentType();
          console.log(res);
        }).catch(err => {
          loading.dismiss();
          console.log(err)
          presentToastCtrl(strapiErrorMsg(err));
        })
      }
    }
  }

}
