import { Component } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { UserData } from '../../providers/user-data';

import { UserOptions } from '../../interfaces/user-options';
import { LoadingController } from '@ionic/angular';
import { checkValidtion, presentToastCtrl, strapiErrorMsg } from '../../shared/utils';
import { UserProvider } from '../../providers/user';



@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
  styleUrls: ['./login.scss'],
})
export class LoginPage {
  private login: FormGroup;
  errormsg:any;

  constructor(
    public userData: UserData, public router: Router, public loadingCtrl: LoadingController,private formBuilder: FormBuilder,
    public userSrv:UserProvider
  ) {
    this.login = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
    });
    if (this.userSrv.getAccessToken()){
      this.router.navigateByUrl('/app/tabs/schedule');
    }
  }

  async onLoginSubmit(form: NgForm) {
    let loading = await this.loadingCtrl.create({ message: 'Please wait...' });
    loading.present()
    this.errormsg =null;
    if (this.login.invalid) {
      console.log(this.login.controls)
      this.errormsg = checkValidtion(this.login.controls);
      loading.dismiss();
      return;
    }
    let user = JSON.parse(JSON.stringify(this.login.value))
    user.identifier = user.email;
    this.userSrv.login(user).then(res => {
      loading.dismiss();
      this.userData.login(user.username);
      this.router.navigateByUrl('/app/tabs/schedule');
    }).catch(err => {
      
      this.errormsg = strapiErrorMsg(err.error);
      console.log(err.error,this.errormsg)
      presentToastCtrl(this.errormsg || "Something went wrong");
      loading.dismiss();
    })
  }
}
