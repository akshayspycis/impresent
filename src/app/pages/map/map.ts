import { Component, ElementRef, Inject, ViewChild, AfterViewInit } from '@angular/core';
import { ConferenceData } from '../../providers/conference-data';
import { Platform } from '@ionic/angular';
import { DOCUMENT } from '@angular/common';

import { darkStyle } from './map-dark-style';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';

@Component({
  selector: 'page-map',
  templateUrl: 'map.html',
  styleUrls: ['./map.scss']
})
export class MapPage implements AfterViewInit {
  @ViewChild('mapCanvas', { static: true }) mapElement: ElementRef;
  assignment: any;
  title: string;
  currentdate: any;
  constructor(
    @Inject(DOCUMENT) private doc: Document,
    public confData: ConferenceData,
    public platform: Platform, private route: ActivatedRoute, private router: Router) {

    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.assignment = this.router.getCurrentNavigation().extras.state.assignment;
        console.log(this.assignment);
        this.title = this.assignment.client.name;
        this.openMap(this.assignment.client.geo);
        this.currentdate = new Date();
      }
    });
  }

  async ngAfterViewInit() {

  }

  async openMap(mapData) {
    const appEl = this.doc.querySelector('ion-app');
    let isDark = false;
    let style = [];
    if (appEl.classList.contains('dark-theme')) {
      style = darkStyle;
    }

    const googleMaps = await this.getGoogleMaps(
      'AIzaSyAZEN-NDQKhjABJCM8cbyK_CnxT2hujPq8'
    );

    let map;

    console.log(mapData)
    const mapEle = this.mapElement.nativeElement;

    map = new googleMaps.Map(mapEle, {
      center: mapData,
      zoom: 16,
      styles: style
    });


    googleMaps.event.addListenerOnce(map, 'idle', () => {
      mapEle.classList.add('show-map');
    });

    const observer = new MutationObserver((mutations) => {
      mutations.forEach((mutation) => {
        if (mutation.attributeName === 'class') {
          const el = mutation.target as HTMLElement;
          isDark = el.classList.contains('dark-theme');
          if (map && isDark) {
            map.setOptions({ styles: darkStyle });
          } else if (map) {
            map.setOptions({ styles: [] });
          }
        }
      });
    });
    observer.observe(appEl, {
      attributes: true
    });
  }


  getGoogleMaps(apiKey: string): Promise<any> {
    const win = window as any;
    const googleModule = win.google;
    if (googleModule && googleModule.maps) {
      return Promise.resolve(googleModule.maps);
    }

    return new Promise((resolve, reject) => {
      const script = document.createElement('script');
      script.src = `https://maps.googleapis.com/maps/api/js?key=${apiKey}&v=3.31`;
      script.async = true;
      script.defer = true;
      document.body.appendChild(script);
      script.onload = () => {
        const googleModule2 = win.google;
        if (googleModule2 && googleModule2.maps) {
          resolve(googleModule2.maps);
        } else {
          reject('google maps not available');
        }
      };
    });
  }

  onTouchOfAttendance(viewtype) {
    let navigationExtras: NavigationExtras = { state: { assignment: this.assignment, viewtype: viewtype } };
    this.router.navigate(['attendance'], navigationExtras);
  }

}