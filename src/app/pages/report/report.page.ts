import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, Config, IonRouterOutlet, LoadingController, ModalController, ToastController } from '@ionic/angular';
import * as moment from 'moment';
import { ConferenceData } from '../../providers/conference-data';
import { ContentTypesProvider } from '../../providers/content-types';
import { isNotEmpty } from '../../shared/utils';
@Component({
  selector: 'app-report',
  templateUrl: './report.page.html',
  styleUrls: ['./report.page.scss'],
})
export class ReportPage implements OnInit {
  all_days: any[];
  _fromdate: any;
  _todate: any;
  isdisplay: boolean = false;
  attendances: any[];
  contentId: string = 'application::attendance.attendance';
  constructor(public alertCtrl: AlertController,
    public confData: ConferenceData,
    public loadingCtr: LoadingController,
    public modalCtrl: ModalController,
    public router: Router,
    public routerOutlet: IonRouterOutlet,
    public toastCtrl: ToastController,
    public config: Config,
    public contenttypesPvr: ContentTypesProvider) { }

  ngOnInit() {
    this.all_days = this.getAllDate();
    this._fromdate = moment(this.all_days[0].date).format('YYYY-MM-DD');
    this._todate = moment(this.all_days[this.all_days.length - 1].date).format('YYYY-MM-DD');
    console.log(this._fromdate)
    this.loadAttendance();
    this.isdisplay = true;
  }

  async loadAttendance(event?) {
    let _self = this;
    let loading = await this.loadingCtr.create();
    this.attendances = [];
    if (!event) {
      await loading.present();
    }
    // let filter = [{ key: 'status', value: 'active' }];
    let filter = [{ key: "fromdate_gte", value: this._fromdate }, { key: "fromdate_lte", value: this._todate }];
    this.contenttypesPvr.loadContentTypeRecord(this.contentId, 100, this.attendances.length, filter).then(res => {
      if (isNotEmpty(res)) {
        this.attendances.push(...res);
      }
      this.all_days.forEach(_a => {
        let _o = this.attendances.find(a => a.fromdate == moment(_a.date).format('YYYY-MM-DD'))
        if (_o) {
          let _i = moment(_o.fromdate + " " + _o.intime).format('hh:mm A');
          let _ou = moment(_o.fromdate + " " + _o.outtime).format('hh:mm A');
          let _dff=moment.utc(moment(_o.fromdate + " " + _o.outtime).diff(moment(_o.fromdate + " " + _o.intime))).format("HH:mm:ss")
          console.log("_i", _i, _ou)
          _a.msg = `${_i} to ${_ou}`;
          _a.dff=_dff;
        }
      })
      console.log(res);
      loading.dismiss();
      if (event) event.target.complete();
    }).catch(err => {
      loading.dismiss();
      if (event) event.target.complete();
    })
  }

  getAllDate() {
    let date = new Date();
    let month = date.getMonth() + 1;
    date.setDate(1);
    date.setMonth(month)
    console.log(date, month)
    let all_days = [];
    while (date.getMonth() == month) {
      let d = date.getFullYear() + '-' + date.getMonth().toString().padStart(2, '0') + '-' + date.getDate().toString().padStart(2, '0');
      all_days.push({ date: d });
      date.setDate(date.getDate() + 1);
    }
    all_days = all_days.map(a => { return { date: new Date(a.date) } });
    console.log(all_days);

    return all_days;
  }

}
