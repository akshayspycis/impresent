import { Component, ViewChild, OnInit } from '@angular/core';
import { NavigationEnd, NavigationExtras, Router } from '@angular/router';
import { AlertController, IonList, IonRouterOutlet, LoadingController, ModalController, ToastController, Config } from '@ionic/angular';

import { ScheduleFilterPage } from '../schedule-filter/schedule-filter';
import { ConferenceData } from '../../providers/conference-data';
import { UserData } from '../../providers/user-data';
import { ContentTypesProvider } from '../../providers/content-types';
import { isNotEmpty, presentToastCtrl } from '../../shared/utils';
import * as moment from 'moment';
import { UserProvider } from '../../providers/user';
@Component({
  selector: 'page-schedule',
  templateUrl: 'schedule.html',
  styleUrls: ['./schedule.scss'],
})
export class SchedulePage implements OnInit {
  // Gets a reference to the list element
  @ViewChild('scheduleList', { static: true }) scheduleList: IonList;

  dayIndex = 0;
  queryText = '';
  segment = 'today';
  excludeTracks: any = [];
  shownSessions: any = [];
  groups: any = [];
  confDate: string;


  assignments: any[] = [];
  todayassignments: any[] = [];
  contentId: string = 'application::assignment.assignment';
  user: any;
  _limit: number = 10;
  constructor(
    public alertCtrl: AlertController,
    public confData: ConferenceData,
    public loadingCtr: LoadingController,
    public modalCtrl: ModalController,
    public router: Router,
    public routerOutlet: IonRouterOutlet,
    public toastCtrl: ToastController,
    public config: Config,
    public contenttypesPvr: ContentTypesProvider,
    public userSvr:UserProvider
  ) { }

  ngOnInit() {
    this.user=this.userSvr.getCurrentUser();
    this.assignments = [];
    this.todayassignments = [];
    this.loadAssignment();
    this.router.events.subscribe((event) => {
      console.log("event.url",event["url"])
      if (event instanceof NavigationEnd && event.url === '/app/tabs/schedule') {
        console.log("ionViewWillEnter")
        this.assignments = [];
        this.todayassignments = [];
        this.loadAssignment();
      }
     });
  }

  onTouchOfSchedule(assignment) {
    if(moment().isBetween(moment(assignment.fromdate), moment(assignment.todate))){
      if(assignment.status!="completed" && assignment.status!="cancelled"){
        let navigationExtras: NavigationExtras = { state: { assignment: assignment } };
        this.router.navigate(['map'], navigationExtras);
      }
    }else{
      presentToastCtrl('Sorry you are not eligible attend')
    }
  }

  async loadAssignment(event?) {
    let _self = this;
    let loading = await this.loadingCtr.create();
    this.assignments = [];
      this.todayassignments = [];
    if (!event) {
      await loading.present();
    }
    let filter = [{ key: 'employee', value: this.user.id }];
    // let filter = [];
    this.contenttypesPvr.loadContentTypeRecord(this.contentId, 10, this.assignments.length, filter).then(res => {
      if (isNotEmpty(res)) {
        this.assignments.push(...res);
        this.mapTodayAssignments(res);
      }
      console.log(res);
      loading.dismiss();
      if (event) event.target.complete();
    }).catch(err => {
      loading.dismiss();
      if (event) event.target.complete();
    })
  }
  mapTodayAssignments(res) {
    this.todayassignments = res.filter(r => moment().isBetween(moment(r.fromdate), moment(r.todate)));
  }
  segmentChanged(e){
    console.log(e,this.segment)
  }

}
