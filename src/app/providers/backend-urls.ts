import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject } from 'rxjs';
import { PLATFORM_API_URL } from '../shared/constants';

@Injectable()
export class BackendUrlsProvider {

    localGOKServer: string;
    backendUrlObserver: Subject<any> = new BehaviorSubject(null);
    private _apiEndpoints: Array<{ name: string, uri: string }>;

    constructor() {
        this._apiEndpoints = [
            { name: "content-types-app", uri: "/content-manager/content-types/app/:uid" },
            { name: "relation-list", uri: "/content-manager/explorer/app/:model/relation-list/:targetField" },
            { name: "createrecord", uri: "/content-manager/explorer/app/:model" },
            { name: "updaterecord", uri: "/content-manager/explorer/app/:model/:id" },
            { name: "deleterecord", uri: "/content-manager/explorer/app/:model/:id" },
            { name: "countrecord", uri: "/content-manager/explorer/:model/count" },
            { name: "content-type-record", uri: "/content-manager/explorer/app/:model" },
            { name: "content-type-one-record", uri: "/content-manager/explorer/app/:model/:id" },
            { name: "location", uri: "/location" },
            { name: "uploadimg", uri: "/upload" },
            { name: "deleteimg", uri: "/upload/files/:id" },
            { name: "registration", uri: "/auth/local/register" },
            { name: "authenticated", uri: "/auth/local" },
        ];
    }

    onBackendUrlModification(): Subject<any> {
        return this.backendUrlObserver;
    }

    getLocalGOKServer() {
        return this.localGOKServer;
    }

    getHost() {
        return PLATFORM_API_URL;
    }

    getURL(functioname: string, params?: any) {
        let _self = this;
        return _self.getHost() + _self.getURI(functioname, params);
    }

    getURI(functioname: string, params?: any) {
        let _self = this;
        let uri;
        let api = _self._apiEndpoints.find(api => api.name == functioname);
        if (api) {
            uri = api.uri;
            for (let p in params) {
                if (p && params[p]) {
                    uri = uri.replace(':' + p, params[p])
                }
            }
        }
        return uri;
    }
}
