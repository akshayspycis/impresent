import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { isNotEmpty, isNotSkipField } from '../shared/utils';
import { BackendUrlsProvider } from './backend-urls';
import { StorageProvider } from './storage';
import { TokenProvider } from './token';
import { map } from 'rxjs/operators';
/*
  Generated class for the ContentTypesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ContentTypesProvider {
  contenttypes: any[] = [];

  constructor(public http: HttpClient, public token: TokenProvider, public storage: StorageProvider,
    public backendSrv: BackendUrlsProvider) {
    console.log('Hello ContentTypesProvider Provider');
  }


  loadContentTypeRecord(model, _limit, _start, filters?, _sort = "id DESC") {
    let _self = this;
    let urlparams = new HttpParams().set('_limit', _limit).set('_start', _start).set('_sort', _sort);
    if (isNotEmpty(filters)) {
      filters.forEach(filter => {
        urlparams = urlparams.set(filter.key, filter.value)
      });
    }
    return _self.http.get<any>(_self.backendSrv.getURL("content-type-record", { model: model }), { params: urlparams })
      .toPromise()
  }

  loadContentTypeOneRecord(model, id) {
    let _self = this;
    let urlparams = new HttpParams();
    return _self.http.get<any>(_self.backendSrv.getURL("content-type-one-record", { model: model, id: id }), { params: urlparams })
      .toPromise()
  }

  loadContentType(uid) {
    let _self = this;
    let urlparams = new HttpParams()
    return _self.http.get<any>(_self.backendSrv.getURL("content-types-app", { uid: uid }), { params: urlparams })
      .pipe(map(data => {
        _self.saveAndUpdate(uid, data);
        console.log("data", data)
        return _self.mapContent(uid, data);
        // return data;
      })).toPromise()
  }

  async mapContent(uid, data) {
    const { data: { contentType: { schema: { attributes: attributes } }, components, contentType } } = data;
    let _attributes = {};
    if (attributes) {
      if (isNotEmpty(Object.keys(attributes))) {
        for (let _a of Object.keys(attributes)) {
          let attribute = attributes[_a];
          if (isNotSkipField(_a)) {
            if (attribute.type) {
              if (attribute.type == "relation" && attribute.targetModel != "plugins::users-permissions.user") {
                let results;
                if (contentType.isComponent) {
                  results = await this.loadRelationList(uid, attribute.collection, contentType.uid);
                } else {
                  results = await this.loadRelationList(uid, attribute.model);
                }
                if (isNotEmpty(results)) attribute.results = results;
              } else if (attribute.type == "component") {
                if (components && components[attribute.component]) {
                  attribute.attributes = await this.mapContent(uid, { data: { contentType: components[attribute.component], components } })
                }
              } else if (attribute.type == "enumeration") {
                attribute.results = attribute.enum;
              } else if (attribute.type == "string" || attribute.type == "integer") {
                attribute.value = null;
              }
            }
            _attributes[_a] = attribute;
          }
        }
      }
    }
    return _attributes;
  }


  loadRelationList(uid, targetField, _component = null, _limit = "20", _start = "0") {
    let _self = this;
    let urlparams = new HttpParams().set("_limit", _limit).set("_start", _start)
    if (_component) urlparams = new HttpParams().set("_limit", _limit).set("_start", _start).set("_component", _component);
    return _self.http.get<any>(_self.backendSrv.getURL("relation-list", { model: uid, targetField: targetField }), { params: urlparams })
      .pipe(map(res => { return res; }))
      .toPromise()
  }

  saveAndUpdate(uid, data) {
    if (this.contenttypes.indexOf(o => o.uid == uid) == -1) {
      this.contenttypes.push({ uid: uid, content: data })
    }
  }

  modifyDataFormContentType(contenttype) {
    let obj = {}
    if (isNotEmpty(Object.keys(contenttype))) {
      for (let _a of Object.keys(contenttype)) {
        let attribute = contenttype[_a];
        if (isNotSkipField(_a)) {
          if (attribute.type) {
            if (attribute.type == "relation") {
              if (attribute.isComponent) {
                obj[_a] = this.modifyDataFormContentType(attribute.attributes);
              } else {
                if (isNotEmpty(attribute.values)) {
                  obj[_a] = attribute.values.map(r => r.id);
                } else {
                  obj[_a] = attribute.value;
                }
              }
            } else if (attribute.type == "component") {
              obj[_a] = this.modifyDataFormContentType(attribute.attributes);
            } else if (attribute.type == "enumeration") {
              obj[_a] = attribute.value;
            } else if (attribute.type == "string" || attribute.type == "integer" || attribute.type == "richtext" || attribute.type == "json") {
              obj[_a] = attribute.value;
            } else if (attribute.type == "media") {
              if (attribute.multiple) {
                if (isNotEmpty(attribute.values)) {
                  obj[_a] = attribute.values.filter(o => o.id).map(o => o.id);
                }
              } else if (attribute.value) {
                obj[_a] = attribute.value.id;
              }
            } else if (attribute.type == "date" && attribute.value) {
              obj[_a] = new Date(attribute.value);
            } else if (attribute.type == "time" && attribute.value) {
              obj[_a] = attribute.value;
            }
          }
        }
      }
    }
    return obj;
  }

  convertFilter(data, _query?) {
    let query = _query || {}
    if (isNotEmpty(Object.keys(data))) {
      for (let k of Object.keys(data)) {
        let v = data[k];
        if (v) {
          if (typeof v == "string" || typeof v == "number") {
            query[k] = v;
          } else if (typeof v == "object") {
            if (Array.isArray(v)) {
              if (isNotEmpty(v)) {
                query['_where'] = { _or: [] }
                v.forEach(o => {
                  if (typeof o == "string" || typeof o == "number") {
                    let _co = {};
                    _co[k] = o;
                    query['_where']["_or"].push(_co);
                  }
                })
              }
            } else {
              let _o = this.convertFilter(v, {});
              if (isNotEmpty(Object.keys(_o))) {
                query[k] = _o
              }
            }
          }
        }
      }
    }
    return query;
  }

  uploadImage(formData) {
    let _self = this;
    let urlparams = new HttpParams();
    return _self.http.post<any>(_self.backendSrv.getURL("uploadimg", {}), formData, { params: urlparams })
      .pipe(map(res => { return res; }))
      .toPromise()
  }

  saveContent(data, model) {
    let _self = this;
    let urlparams = new HttpParams();
    return _self.http.post<any>(_self.backendSrv.getURL("createrecord", { model: model }), data, { params: urlparams })
      .pipe(map(res => { return res; }))
      .toPromise()
  }

  updateContent(data, model, id) {
    let _self = this;
    let urlparams = new HttpParams();
    return _self.http.put<any>(_self.backendSrv.getURL("updaterecord", { model: model, id: id }), data, { params: urlparams })
      .pipe(map(res => { return res; }))
      .toPromise()
  }

  countContent(model, _limit, _start, filters?, _sort = "id DESC") {
    let _self = this;
    let urlparams = new HttpParams();
    if (_limit) urlparams = urlparams.set('_limit', _limit)
    if (_start) urlparams = urlparams.set('_start', _start)
    if (_sort) urlparams = urlparams.set('_sort', _sort);
    if (isNotEmpty(filters)) {
      filters.forEach(filter => {
        urlparams = urlparams.set(filter.key, filter.value)
      });
    }
    return _self.http.get<any>(_self.backendSrv.getURL("countrecord", { model: model }), { params: urlparams })
      .toPromise()
  }

  deleteContent(model, id) {
    let _self = this;
    let urlparams = new HttpParams();
    return _self.http.delete<any>(_self.backendSrv.getURL("deleterecord", { model: model, id: id }), { params: urlparams })
      .pipe(map(res => { return res; }))
      .toPromise()
  }

  removeImage(id) {
    let _self = this;
    let urlparams = new HttpParams();
    return _self.http.delete<any>(_self.backendSrv.getURL("deleteimg", { id: id }), { params: urlparams })
      .pipe(map(res => { return res; }))
      .toPromise()
  }

}
