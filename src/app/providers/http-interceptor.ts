import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Observable } from "rxjs";
import { PLATFORM_API_URL } from '../shared/constants';
import { TokenProvider } from './token';
import { UserProvider } from './user';
import { tap } from 'rxjs/operators';
import { presentToastCtrl } from '../shared/utils';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    constructor(public token: TokenProvider, public userPvr: UserProvider) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // if (req.url.startsWith(PLATFORM_API_URL)) {
            console.log("this.token.getToken()",this.token.getToken())
        if (this.token.getToken()) {
            req = req.clone({
                setHeaders: {
                    Authorization: `Bearer ${this.token.getToken()}`,
                },
                setParams: {
                }
            });
        } else {
            req = req.clone({
                setHeaders: {
                }
            });
        }
        return next.handle(req).pipe(
            tap((event: HttpEvent<any>) => {
                if (event instanceof HttpResponse && event.status === 201) {
                    console.log(event)
                    // presentToastCtrl(event);
                }
            })
        );
    }
}