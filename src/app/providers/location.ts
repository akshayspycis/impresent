import { Injectable } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { StorageProvider } from './storage';

/*
  Generated class for the LocationProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LocationProvider {
  coords: any;

  constructor(public storage: StorageProvider, private geolocation: Geolocation) {
    console.log('Hello LocationProvider Provider');
    let watch = this.geolocation.watchPosition();
    watch.subscribe((data: any) => {
      this.coords = data.coords;
    });
  }

  async getCurrentPosition() {
    if (this.coords) {
      return this.coords;
    } else {
      this.geolocation.getCurrentPosition().then((resp) => {
        console.log(resp);
        this.coords = resp.coords;
        return resp.coords;
        // resp.coords.latitude
        // resp.coords.longitude
      }).catch((error) => {
        console.log('Error getting location', error);
      });
    }

  }

}
