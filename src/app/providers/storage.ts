import { Injectable } from '@angular/core';
import { StorageDriver } from '../shared/storage.driver';

@Injectable()
export class StorageProvider {

  prefix = 'impresent.';

  constructor() { }

  set(key: string, value: any) {
    let _self = this;
    if (key && value) {
      if (value instanceof Object || Array.isArray(value)) {
        _self.save(key, JSON.stringify(value));
      } else {
        _self.save(key, value);
      }
    }
  }

  save(key: string, value: string): void {
    let _self = this;
    StorageDriver.set(`${_self.prefix}${key}`, value);
  }

  remove(key: string) {
    let _self = this;
    StorageDriver.remove(`${_self.prefix}${key}`);
  }

  get(key: string) {
    let _self = this;
    let value = null;
    value = StorageDriver.get(`${_self.prefix}${key}`);
    if (value) {
      try {
        value = JSON.parse(value);
      } catch (err) { }
    }
    return value;
  }

  clear() {
    let r = null;
    try { StorageDriver.clear(); } catch (er) { }
    return r
  }

}
