import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { StorageProvider } from './storage';

/*
  Generated class for the TokenProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class TokenProvider {
  jwt: any;

  constructor(public http: HttpClient,public storage: StorageProvider) {
    console.log('Hello TokenProvider Provider');
  }

  async loadFromStorage() {
    console.log("this.storage.get('jwt')",this.storage.get('jwt'))
    this.jwt = await this.storage.get('jwt');
  }
  
  setAccessToken(token) {
    this.jwt = token;
    this.storage.set('jwt', this.jwt);
    if (!this.isValidToken()) {
      console.log("new token is not valid so removing it");
      this.removeToken();
    }
  }

  removeToken() {
    this.jwt = null;
    this.storage.remove('jwt');
  }

  getToken() {
    if (this.isValidToken() && this.jwt) {
      return this.jwt;
    }
    return null;
  }

  getExpiryDate() {
    if (this.isValidToken() && this.jwt) {
      return this.jwt;
    }
    return null;
  }

  isValidToken() {
    if (this.jwt ) {
      // let now: Date = new Date();
      // let createdAt: Date = new Date(this.accesstoken.created)
      // let expiryDate = new Date((createdAt.getTime() + (this.accesstoken.ttl * 1000)));
      // return now < expiryDate;
      return true
    } else { return false; }
  }
}
