import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as moment from 'moment';
// import { PushNotificationProvider } from '../push-notification/push-notification';
import { Subject, BehaviorSubject } from 'rxjs';
import { User } from '../modal/modal';
import { PLATFORM_API_URL } from '../shared/constants';
import { isNotBlank, isNotEmpty, log, trim } from '../shared/utils';
import { BackendUrlsProvider } from './backend-urls';
import { ContentTypesProvider } from './content-types';
import { StorageProvider } from './storage';
import { TokenProvider } from './token';
import { map } from 'rxjs/operators';


@Injectable()
export class UserProvider {

  user: User;
  userSubject: Subject<User> = new BehaviorSubject(null);
  _expiryTimeoutId: any;
  wishlist: any;
  wishlistSub: Subject<any> = new BehaviorSubject(null);
  constructor(public http: HttpClient, public token: TokenProvider, public storage: StorageProvider,
    public backendSrv: BackendUrlsProvider, public contenttypesPvr: ContentTypesProvider) {

  }


  async initialize() {
    let _self = this;
    await _self.token.loadFromStorage();
    _self.userSubject.next(await _self.loadFromLocalStorage());
    _self.save();
  }

  onUserInfoChange() {
    return this.userSubject;
  }

  getAccessToken() {
    if (this.token.isValidToken()) return this.token.getToken();
    return null;
  }

  setEmail(email) {
    this.user.email = email;
    this.save();
  }

  async loadFromLocalStorage() {
    try {
      let jwt = await this.storage.get("jwt");
      let user = await this.storage.get("user");
      this.user = new User(user);
      this.user.jwt = jwt;
    } catch (er) { log(er) }
    return this.user;
  }

  public logout() {
    const _self = this;
    log("Logout is triggered");
    if (_self.user.jwt) {
      _self.user.jwt = null;
      _self.clearData();
      _self.user = new User(null);
      _self.userSubject.next(_self.user);
      _self.token.setAccessToken(null);
    }
    return _self.user;
  }

  public registration(body) {
    let _self = this;
    var body = { ...body };
    let urlparams = new HttpParams();
    return _self.http.post<any>(_self.backendSrv.getURL("registration", {}), body, { params: urlparams })
      .pipe(map((data: any) => {
        if (data && data.jwt) {
          _self.user = new User(data.user);
          _self.user.jwt = data.jwt;
          _self.save();
          _self.userSubject.next(_self.user);
          _self._activityAfterLoggedin(_self.user);
          return _self.user;
        }
        return data;
      })).toPromise();
  }

  public login(body) {
    let _self = this;
    let urlparams = new HttpParams()
    return _self.http.post<any>(_self.backendSrv.getURL("authenticated", {}), body, { params: urlparams })
      .pipe(map((data: any) => {
        console.log(data)
        if (data && data.jwt) {
          _self.user = new User(data.user);
          _self.user.jwt = data.jwt;
          _self.save();
          _self.userSubject.next(_self.user);
          _self._activityAfterLoggedin(_self.user);
          return _self.user;
        }
        return data;
      })).toPromise();
  }


  private _activityAfterLoggedin(user) {
    let _self = this;
    // _self.pushNotifier.installation(user)
    //   .then(installation => { })
    //   .catch(err => console.error('Error in installtaion registry', err));
  }

  fetchEmailDomain(email) {
    let emailDomainPattern = /@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    let r = emailDomainPattern.exec(email);
    if (r.length > 1) return r[1];
    return null;
  }


  public save(): void {
    const _self = this;
    if (_self.user) {
      console.log(_self.user)
      _self.storage.set('jwt', trim(_self.user.jwt));
      _self.storage.set('user', trim(JSON.stringify(_self.user)));
      if (_self.user && _self.user.jwt) {
        _self.token.setAccessToken(_self.user.jwt)
      } else { _self.token.removeToken() }
    }
  }

  public update(): void {
    let _self = this;
    if (_self.user) {
      _self.storage.set('jwt', trim(_self.user.jwt));
      _self.storage.set('user', trim(JSON.stringify(_self.user)));
      if (!_self.user.jwt) {
        _self.storage.get('jwt')
          .then(d => {
            _self.user.jwt = d
          }).catch(e => { console.error(e) });
      }
    }
  }


  public clearData() {
    const _self = this;
    log("Clearing all customer's data");
    _self.storage.remove('user');
    _self.storage.remove('jwt');
    _self.token.removeToken();
  }


  getCurrentUser() {
    if (!this.user) { return new User(null) }
    return this.user;
  }


}
