/**
 * Created by Arun Yokesh Kumar on 05-12-2016.
 */

export const PLATFORM_API_URL: string = 'https://impresent-api.herokuapp.com';
// export const PLATFORM_API_URL: string = 'http://localhost:1337';

export const FIELD_VALIDATION: any[] = [
    { type: "name", error: "Name should not be blank." },
    { type: "address", error: "Please provide valid address." },
    { type: "geo", error: "Please provide geo location." },
];
