import { FIELD_VALIDATION } from './constants';
import qs from 'qs';
import { async } from '@angular/core/testing';

let toastCtrl;

export function setToastCtrlObj(value: any) {
    toastCtrl = value;
}

export function trim(value: string) {
    if (value) {
        return value.trim();
    }
    return value;
}

export function isNotEmpty(array) {
    if (array && Array.isArray(array) && array.length > 0) {
        return true;
    }
    return false;
}

export function isNotSkipField(attribute) {
    switch (attribute) {
        case 'id':
            return false;
        case 'adminuser':
            return false;
        case 'updatedAt':
            return false;
        case 'createdAt':
            return false;
        default:
            return true;
    }
}

export function isEmpty(array) {
    if (!array || (Array.isArray(array) && array.length == 0)) {
        return true;
    }
    return false;
}

export function isObjectEmpty(obj) {
    if (Object.keys(obj).length == 0) {
        return true;
    }
    return false;
}

export function isNotBlank(value: string): boolean {
    return (value && trim(value) != '');
}

export function isBlank(value: string): boolean {
    return (!value || trim(value) == '');
}

export function addUniqueEntry(array: any[], data: any) {
    if (data && data.id && Array.isArray(array)) {
        let index = array.findIndex(a => (a && a.id == data.id));
        if (index == -1) {
            array.push(data);
        } else { array[index] = data }
    }
    return array;
}

export function addUniqueEntries(array: any[], array2: any[]) {
    if (isNotEmpty(array2)) {
        array2.forEach(d => {
            addUniqueEntry(array, d);
        });
    }
    return array;
}

export function replace(str, ...values) {
    values.forEach((v, index) => {
        if (!v) { v = ''; }
        str = str.replace('{' + index + '}', v);
    })
    return str;
}

export function isJson(str) {
    if (str && typeof str === "string") {
        try {
            JSON.parse(str);
            return true;
        } catch (e) {
            return false;
        }
    }
    return false;
}


export function toNumber(value) {
    let result = Number(value);
    if (isNaN(result)) { return 0; }
    return result;
}

export function doubleDecimal(value: number) {
    return toNumber(value.toFixed(2));
}


export function fetchEmailDomain(email) {
    let emailDomainPattern = /@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    let r = emailDomainPattern.exec(email);
    if (r && r.length > 1) return r[1];
    return null;
}

export function log(...data: any[]) {
    if (isNotEmpty(data)) {
        console.log(data.join(" "));
        // if (Bugfender && !IS_BROWSER && Bugfender.log instanceof Function) Bugfender.log(data.join(" "));
    } else {
        console.log(data)
    }
}

export function isNotNullAndUndefined(value) {
    return value != null && value != undefined;
}


export function _checkValidtion(m, controls, key) {
    if (
        (controls[key] && controls[key]["status"] && controls[key]["status"] == "INVALID") ||
        (controls[key] && controls[key]["required"] && controls[key]['relationType'] == "manyWay" && isEmpty(controls[key]["values"])) ||
        (controls[key] && controls[key]["required"] && controls[key]['type'] == "media" && controls[key]['multiple'] && isNotEmpty(controls[key]["values"]) && !controls[key]["values"].some(o => o.id)) ||
        (controls[key] && controls[key]["required"] && controls[key]['type'] == "media" && !controls[key]['multiple'] && controls[key]["value"] && !controls[key]["value"].id) ||
        (controls[key] && controls[key]["required"] && controls[key]["type"] == "json" && controls[key]["value"] == null)||
        (controls[key] && controls[key]["required"] &&
            (
                (controls[key]["type"] == "boolean" && !isNotNullAndUndefined(controls[key]["value"])) ||
                (controls[key]["type"] != "boolean" && controls[key]["type"] != "json" && controls[key]['type'] != "media" && isBlank(controls[key]["value"]))

            )
        )
    ) {
        let o = FIELD_VALIDATION.find(o => o.type == key);
        if (o) m.push(o.error);
    }
}

export function checkValidtion(controls, filed?) {
    let m = [];
    if (isNotEmpty(filed)) {
        if (Object.keys(controls).length > 0) {
            for (let key of Object.keys(controls)) {
                if (controls[key].type && controls[key].type == "component") {
                    let msg = checkValidtion(controls[key].attributes, filed);
                    if (msg) m.push(msg);
                } else {
                    if (filed.includes(key)) {
                        _checkValidtion(m, controls, key);
                    }
                }
            }
        }
    } else {
        if (Object.keys(controls).length > 0) {
            for (let key of Object.keys(controls)) {
                if (controls[key].type && controls[key].type == "component") {
                    let msg = checkValidtion(controls[key].attributes);
                    if (msg) m.push(msg);
                } else {
                    _checkValidtion(m, controls, key);
                }
            }
        }
    }
    return isNotEmpty(m) ? m.join("\n") : null;
}



export function strapiErrorMsg(err) {
    let errormsg = "";
    if (err && isNotEmpty(err.message)) {
        err.message.forEach(_messages => {
            if (isNotEmpty(_messages.messages)) {
                _messages.messages.forEach(m => {
                    if (m.message) {
                        if (errormsg != "") errormsg += "," + m.message;
                        else errormsg += m.message;
                    }
                })
            }
        })
    }
    return errormsg;
}


export async function presentToastCtrl(msg, isSuccess?, showCloseButton = true) {
    let alert = await toastCtrl.create({
        message: msg,
        duration: 1000 * 5,
        showCloseButton: showCloseButton,
        position: 'middle',
        cssClass: isSuccess ? 'impresent-success-toast' : 'shake-move',
    });
    await alert.present();
}

export function whereStringify(filter) {
    let _q = [];
    filter = qs.stringify(filter);
    filter = decodeURIComponent(filter);
    filter.split('&').forEach(o => {
        _q.push({ key: o.split("=")[0], value: o.split("=")[1] })
    })
    return _q;
}
